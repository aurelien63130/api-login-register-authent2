<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');

    }

    public function load(ObjectManager $manager): void
    {
        $array = [$this->getReference("aurelien"), $this->getReference("morgan"),
            $this->getReference("victor")];

        for ($i=0; $i<49; $i++){
            $post = new Post();
            $post->setTitre($this->faker->sentence());
            $manager->persist($post);

            $randAuteur = rand(0,2);
            $post->setAuteur($array[$randAuteur]);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [AuteurFixtures::class];
    }
}

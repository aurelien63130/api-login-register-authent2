<?php

namespace App\DataFixtures;

use App\Entity\Auteur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AuteurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $auteur = new Auteur();
        $auteur->setNom("Delorme");
        $auteur->setPrenom("Aurélien");

        $this->addReference("aurelien", $auteur);

        $auteur2 = new Auteur();
        $auteur2->setPrenom("Morgan");
        $auteur2->setNom("Collin");

        $this->addReference("morgan", $auteur2);

        $auteur3 = new Auteur();
        $auteur3->setPrenom("Victor");
        $auteur3->setNom("Hugo");

        $this->addReference("victor", $auteur3);

        $manager->persist($auteur);
        $manager->persist($auteur2);
        $manager->persist($auteur3);

        $manager->flush();
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}

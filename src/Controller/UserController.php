<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    private $serializer;
    private $headers = ['Content-Type'=> 'application/json'];
    private $hasher;
    private $em;

    public function __construct(SerializerInterface $serializer, UserPasswordHasherInterface $hasher,
    EntityManagerInterface $em){
        $this->serializer = $serializer;
        $this->hasher = $hasher;
        $this->em = $em;
    }

    #[Route('/api/current-user', name: 'user', methods: ['GET'])]
    public function index(): Response
    {
        return new Response($this->serializer->serialize($this->getUser(), 'json'),Response::HTTP_OK, $this->headers);
    }

    #[Route('/api/register', name: 'register_user', methods: ['POST'])]
    public function register(Request $request){
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(UserType::class, new User());
        $form->submit($data);
        if($form->isValid()){
            $user = $form->getData();
            $user->setPassword($this->hasher->hashPassword($user, $user->getPassword()));
            $this->em->persist($user);
            $this->em->flush();

            return new Response($this->serializer->serialize($user, 'json'),Response::HTTP_OK, $this->headers);
        } else {
            return new Response($this->serializer->serialize($form->getErrors(true, true), 'json'),
                Response::HTTP_BAD_REQUEST, $this->headers);
        }
    }
}

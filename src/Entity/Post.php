<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"auteur": "partial", "titre": "partial"})
 */
#[ApiResource(
    normalizationContext: ['groups' => ['read_post_auteur']],
    denormalizationContext: ['groups' => ['write_post_auteur']],
)]
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read_post_auteur"})
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read_post_auteur","write_post_auteur"})
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity=Auteur::class, inversedBy="articles", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read_post_auteur", "write_post_auteur"})
     */
    private $auteur;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAuteur(): ?Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(?Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }
}
